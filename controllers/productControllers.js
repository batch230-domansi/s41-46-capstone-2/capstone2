const mongoose = require("mongoose");
const Product = require("../models/product.js");



module.exports.addProduct = (reqBody, AdminData) => {
	if(AdminData.isAdmin == true){
		let newProduct = new Product({
	           name: reqBody.name,
	           price: reqBody.price,
	           stocks: reqBody.stocks
		})
		return newProduct.save().then((newProduct, error) => {
			if (error) {
				return error;
			}
			else {
				return newProduct;
			}
		})
	}	
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


module.exports.getActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}


module.exports.getProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}



module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				name: newData.product.name, 
				description: newData.product.description,
				price: newData.product.price,
				isActive: newData.product.isActive,
				stocks: newData.product.stocks,
				orders: newData.product.orders
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId, {isActive: false}).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}