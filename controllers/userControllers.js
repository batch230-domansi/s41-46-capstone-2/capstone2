const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const User = require("../models/user.js");
const Product = require("../models/product.js");



module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((existingUser) => {
      if (existingUser) {
        // Email already exists, return an error message
        let message = Promise.resolve('The email address you are trying to use has already been registered. Please pick another email address.');
        return message;
      }
   return User.findOne({ mobileNo: reqBody.mobileNo })
    .then((existingNumUser) => {
      if (existingNumUser) {
        // Email already exists, return an error message
        let messageNum = Promise.resolve('Sorry, that number is already taken. Please provide a different number or contact the Admin to reset your password');
        return messageNum;
      }
       let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo,
        isAdmin: reqBody.isAdmin,
      })
      let messageSuc = Promise.resolve('Successully registered! Thanks for signing-up!');
      return newUser.save()
        .then((user) => {
          return Promise.resolve(messageSuc);
        })
    })
  })
    .catch((error) => {
      return Promise.resolve(message);
    })
    .catch((error) => {
      return Promise.resolve(messageNum);
    })
}



module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        let messageWrongPass = Promise.resolve('The password you typed in is wrong. Please check and enter the correct password.');
        return messageWrongPass;
      }
    }
  }).catch((error) => {
    console.error(messageWrongPass);
  })
}


module.exports.getProfileA = (userId) =>{
	  return User.findOne({_id: userId}).then(user =>{
               if(user){
               	   user.password = "*******";
               	if (user.mobileNo) {
          const lastFourDigits = user.mobileNo.substr(-4);
          user.mobileNo = "*******" + lastFourDigits;
        }

        return user;  
        }         
               else{
               	let messageIDExs = Promise.resolve('The ID you typed in is wrong. Please check and enter the correct password.');
               return messageIDExs;
               }
	  })
	  .catch((error) => {
      console.log(messageIDExs);
      });
}


module.exports.getProfileB = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result => {
		result.password = "*****";
		response.send(result);
	})
}


module.exports.order = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    Product.findById(request.body.productId)
    .then(async result => {
        if(result.stocks <= 0){
            console.log("Remaining stocks: " + result.stocks);
            return response.send("Product out of stock");
        }
        else if(result.stocks < request.body.quantity){
            console.log("Remaining stocks: " + result.stocks);
            return response.send("We only have " + result.stocks + " remaining stock/stocks left");
        }
        else{
            let productName = await Product.findById(request.body.productId)
            .then(result => result.name);
            let newData = {
                userId: userData.id,
                userEmail: userData.email,
                productId: request.body.productId,
                productName: productName,
                quantity: request.body.quantity
            }
            let productPrice = await Product.findById(request.body.productId)
            .then(result => result.price);
            let priceData = {
                productPrice: parseInt(productPrice)
            }
            console.log(newData);
            let isUserUpdated = await Users.findById(newData.userId)
            .then(user => {
                user.orders.push({
                    totalAmount: parseInt(priceData.productPrice * request.body.quantity),
                    products: {
                        productId: newData.productId,
                        productName: newData.productName,
                        quantity: newData.quantity,
                    }
                })
                user.totalPurchased = parseInt(user.totalPurchased + (priceData.productPrice * request.body.quantity));
                return user.save()
                .then(result => {
                    console.log(result);
                    return true;
                })
                .catch(error => {
                    console.log(error);
                    return false;
                })
            })
            console.log(isUserUpdated);
            let isProductUpdated = await Product.findById(newData.productId)
            .then(product => {
                product.orders.push({
                    userId: newData.userId,
                    userEmail: newData.userEmail,
                    quantity: newData.quantity
                })
                product.stocks = product.stocks - request.body.quantity;
                return product.save()
                .then(result => {
                    console.log(result);
                    return true;
                })
                .catch(error => {
                    console.log(error);
                    return false;
                })
            })
            console.log(isProductUpdated);
            // Ternary operator
            (isUserUpdated == true && isProductUpdated == true)? 
            response.send("Order successfully placed") : response.send(false);
        }
    })
    .catch(error => response.send(error));
}