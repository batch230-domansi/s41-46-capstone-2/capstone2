const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
     { 
		firstName:{
	        type: String,
	        required: [true, "Name is required"]    
    	},
    	lastName:{
	       	type : String,
	       	required: [true,"Last name is required"]
    	},
    	email:{
       	    type: String, 
       	    required: [true,"Email is required"]
    	},
    	password:{
       	    type: String, 
       	    required: [true,"Password is required"]
  	},
  	mobileNo: {
  	    type: String, 
  	    required: [true,"Mobile Number is required"]
  	},
  	isAdmin: {
  	    type: Boolean, 
  	    default: true
  	},
  	orders:[{
           	totalAmount:{
           	type: Number,
           	required:[true, "Total amount is required"]
           },
            	purchasedOn:{
            	type: Date,
           	default: new Date()
           },
           	products:[{
           				productId:{
           					type: String,
           					required:[true, "productId is required"]
           				},
           				productName:{
           					type: String
              				},
              				quantity:{
              					type: Number,
              					required:[true, "quantity is required"]
              				}
           			}]
            }]
	}
) 

module.exports= mongoose.model("User", UserSchema);