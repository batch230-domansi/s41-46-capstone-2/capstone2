const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
    { 
    	name:{
	        type: String,
	        required: [true, "Name is required"]    
    	},
    	description:{
    		type: String
    	},
    	price:{
    		type: Number,
    		required: [true, "Price is required"]
    	},
    	stocks:{
    		type: Number,
    		required: [true, "Stock is required"]
    	},
    	isActive:{
    		type: Boolean,
    		default: true
    	},
    	createdOn: {
    		type: Date,
    		default: new Date()
    	},
    	orders:[{
    		productId:{
    			type: String
    		},
    		userId:{
    			type: String
    		},
    		userEmail:{
    			type: String
    		},
    		quantity:{
    			type: Number
    		},
    		purchasedOn:{
    			type: Date
    		}
    	}]
	}
) 

module.exports = mongoose.model("Product", productSchema);