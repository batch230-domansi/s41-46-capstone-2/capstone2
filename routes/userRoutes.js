const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");


router.post("/register",(request,response) =>{
      userControllers.registerUser(request.body).then(resultFromControllers => response.send(resultFromControllers))

})


router.post("/login", (request,response) => {
      userControllers.loginUser(request.body).then(resultFromControllers => response.send(resultFromControllers))

})


// Body
router.post("/details", (request,response) => {
      const userId = request.params.id;
      userControllers.getProfileA(request.body).then(resultFromControllers => response.send(resultFromControllers))
})

// Token
router.get("/details", auth.verify, userControllers.getProfileB);


router.post("/order", auth.verify, userControllers.order);

module.exports = router;
