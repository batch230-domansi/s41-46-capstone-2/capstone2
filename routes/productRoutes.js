const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

//Creating a product

router.post("/create", auth.verify, (request, response) => {

	const AdminData = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

productControllers.addProduct(request.body, AdminData).then(resultFromControllers => response.send(resultFromControllers))

})

// Get all products
router.get("/all", (request, response) => {
	productControllers.getAllProducts().then(resultFromControllers => response.send(resultFromControllers))
})

// Get all ACTIVE courses
router.get("/active", (request, response) => {
	productControllers.getActiveProducts().then(resultFromControllers => response.send(resultFromControllers))
})


router.get("/:productId", (request, response) => {
	productControllers.getProduct(request.params.productId).then(resultFromControllers => response.send(resultFromControllers))
})

router.patch("/:productId/update", auth.verify, (request,response) => 
{
	const newData = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.updateProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

router.patch("/:productId/archive", auth.verify, (request,response) => 
{
	const productId = request.params.productId;
    const newData = {
		product: request.body, 
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.productId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


module.exports = router;